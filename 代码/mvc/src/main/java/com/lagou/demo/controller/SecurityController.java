package com.lagou.demo.controller;

import com.lagou.demo.service.IDemoService;
import com.lagou.edu.mvcframework.annotations.LagouAutowired;
import com.lagou.edu.mvcframework.annotations.LagouController;
import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
import com.lagou.edu.mvcframework.annotations.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@LagouController
@LagouRequestMapping("/security")
@Security(value = {"zhangsan", "lisi"})
public class SecurityController {


    @LagouRequestMapping("/handler01")
    @Security("lisi")
    public void handler01(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.getWriter().write("lisi access data");
    }

    @LagouRequestMapping("/handler02")
    @Security({"zhangsan", "lisi"})
    public void handler02(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.getWriter().write("lisi/zhangsan access data");
    }

    @LagouRequestMapping("/handler03")
    public void handler03(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.getWriter().write("access data");
    }
}
